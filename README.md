Dummy WebService project
========================

Build:
`./gradlew build`

Run:
`./gradlew run`

Test:
`./gradlew integrationTest`
