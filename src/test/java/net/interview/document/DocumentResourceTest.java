package net.interview.document;

import io.dropwizard.testing.junit.ResourceTestRule;
import net.interview.io.UUIDMessageBodyReader;
import net.interview.io.UUIDMessageBodyWriter;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DocumentResourceTest {

    private ResourceTestRule resourceTestRule;

    @Rule
    public ResourceTestRule setUpResources() {
        DocumentResource resource = new DocumentResource();

        resourceTestRule = ResourceTestRule.builder()
                                        .addProvider(UUIDMessageBodyReader.class)
                                        .addProvider(UUIDMessageBodyWriter.class)
                                        .addResource(resource)
                                        .build();
        return resourceTestRule;
    }

    private Client client;

    @Before
    public void setUp() {
        client = resourceTestRule.client();
        client.register(UUIDMessageBodyReader.class);
    }

    @Test public void uploadAndGetDocument() throws Exception {
        final String docContent = "{\"text\": \"What's up?\"}";
        final Response response = client.target("http://localhost")
                                        .path("documents")
                                        .request()
                                        .post(Entity.entity(docContent, MediaType.APPLICATION_JSON));
        assertEquals(HttpStatus.OK_200, response.getStatus());
        final UUID id = response.readEntity(UUID.class);
        assertNotNull(id);
        final Response responseGet = client.target("http://localhost")
                                        .path("documents/{id}").resolveTemplate("id", id.toString())
                                        .request(MediaType.APPLICATION_JSON_TYPE)
                                        .get();
        assertEquals(HttpStatus.OK_200, responseGet.getStatus());
        final Document document = responseGet.readEntity(Document.class);
        assertNotNull(document);
        assertEquals(id, document.getId());
        assertEquals(docContent, document.getContent());
    }

    @Test public void getDocuments() throws Exception {
        final Response response = client.target("http://localhost")
                                        .path("documents")
                                        .request(MediaType.APPLICATION_JSON_TYPE)
                                        .get();
        assertEquals(HttpStatus.OK_200, response.getStatus());
        assertNotNull(response.readEntity(new GenericType<Set<UUID>>() {}));

    }

    @Test public void deleteDocument() throws Exception {
        final String docContent = "{\"text\": \"What's up?\"}";
        final Response response = client.target("http://localhost")
                                        .path("documents")
                                        .request()
                                        .post(Entity.entity(docContent, MediaType.APPLICATION_JSON));
        assertEquals(HttpStatus.OK_200, response.getStatus());
        final UUID id = response.readEntity(UUID.class);
        assertNotNull(id);
        final Response responseDelete = client.target("http://localhost")
                                        .path("documents/{id}").resolveTemplate("id", id.toString())
                                        .request(MediaType.APPLICATION_JSON_TYPE)
                                        .delete();
        assertEquals(HttpStatus.OK_200, responseDelete.getStatus());
        final Document document = responseDelete.readEntity(Document.class);
        assertNotNull(document);
        assertEquals(id, document.getId());
        assertEquals(docContent, document.getContent());
        final Response responseGet = client.target("http://localhost")
                                        .path("documents/{id}").resolveTemplate("id", id.toString())
                                        .request(MediaType.APPLICATION_JSON_TYPE)
                                        .get();
        assertEquals(HttpStatus.NOT_FOUND_404, responseGet.getStatus());
    }

}
