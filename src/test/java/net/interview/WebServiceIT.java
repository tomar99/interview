package net.interview;

import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.testing.junit.DropwizardAppRule;
import io.dropwizard.util.Duration;
import net.interview.document.Document;
import net.interview.io.UUIDMessageBodyReader;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;
import java.util.UUID;

import static io.dropwizard.testing.ResourceHelpers.resourceFilePath;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class WebServiceIT {

    @ClassRule
    public static final DropwizardAppRule<WebServiceConfiguration> RULE = new DropwizardAppRule<>(WebService.class, resourceFilePath("WebServiceIT.yml"));

    @Rule
    public TestName name = new TestName();

    private Client client;

    @Before
    public void setUp() {
        final JerseyClientConfiguration config = new JerseyClientConfiguration();
        config.setConnectionTimeout(Duration.minutes(1));
        config.setTimeout(Duration.minutes(1));

        client = new JerseyClientBuilder(RULE.getEnvironment()).using(config).build(name.getMethodName());
        client.register(UUIDMessageBodyReader.class);
    }

    @Test
    public void getAll() {
        final Response response = client.target(format("http://localhost:%d", RULE.getLocalPort()))
                                        .path("documents")
                                        .request(MediaType.APPLICATION_JSON_TYPE)
                                        .get();
        assertEquals(HttpStatus.OK_200, response.getStatus());
        assertNotNull(response.readEntity(new GenericType<Set<UUID>>() {}));
    }

    @Test
    public void upload() {
        final String docContent = "{\"text\": \"What's up?\"}";
        final Response response = client.target(format("http://localhost:%d", RULE.getLocalPort()))
                                        .path("documents")
                                        .request()
                                        .post(Entity.entity(docContent, MediaType.APPLICATION_JSON));
        assertEquals(HttpStatus.OK_200, response.getStatus());
        final UUID id = response.readEntity(UUID.class);
        assertNotNull(id);
        final Response responseGet = client.target(format("http://localhost:%d", RULE.getLocalPort()))
                                        .path("documents/{id}").resolveTemplate("id", id.toString())
                                        .request(MediaType.APPLICATION_JSON_TYPE)
                                        .get();
        assertEquals(HttpStatus.OK_200, responseGet.getStatus());
        final Document document = responseGet.readEntity(Document.class);
        assertNotNull(document);
        assertEquals(id, document.getId());
        assertEquals(docContent, document.getContent());
    }


}
