package net.interview.document;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The document resource handles creation, access and deletion of json documents.
 */
@Path("/documents")
public class DocumentResource {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentResource.class);

    final Map<UUID, Document> documents = new ConcurrentHashMap<>();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Metered
    @ExceptionMetered
    public Response uploadDocument(@QueryParam("title") String prename
                                    , final InputStream documentStream) {
        final Document document = new Document(documentStream);
        documents.put(document.getId(), document);
        LOG.info("Created document with id {}.", document.getId());
        return Response.ok(document.getId().toString()).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Metered
    @ExceptionMetered
    public Response getDocument(@PathParam("id") UUID id) {
        final Document document = documents.get(id);
        if (document != null) {
            return Response.ok(document, MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(HttpStatus.NOT_FOUND_404).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Metered
    @ExceptionMetered
    public Response getDocuments() {
        return Response.ok(documents.keySet(), MediaType.APPLICATION_JSON_TYPE).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Metered
    @ExceptionMetered
    public Response deleteDocument(@PathParam("id") UUID id) {
        final Document document = documents.remove(id);
        if (document != null) {
            return Response.ok(document, MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(HttpStatus.NOT_FOUND_404).build();
    }

}
