package net.interview.document;

import javax.validation.constraints.NotNull;
import java.io.InputStream;
import java.util.Scanner;
import java.util.UUID;

public class Document {

    @NotNull
    private UUID id;
    @NotNull
    private String content;

    public Document() {
    }

    public Document(final InputStream documentStream) {
        this.id = UUID.randomUUID();
        //see http://stackoverflow.com/a/5445161/4704077
        Scanner s = new Scanner(documentStream).useDelimiter("\\A");
        content = s.hasNext() ? s.next() : "";
    }

    public UUID getId() {
        return id;
    }

    public void setId(@NotNull UUID id) {
        if (id == null) {
            throw new IllegalArgumentException("id must not be null.");
        }
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Document document = (Document) o;

        return id.equals(document.id);

    }

    @Override public int hashCode() {
        return id.hashCode();
    }
}
