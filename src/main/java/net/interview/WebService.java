package net.interview;

import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import net.interview.document.DocumentResource;
import net.interview.io.UUIDMessageBodyReader;
import net.interview.io.UUIDMessageBodyWriter;

/**
 * Main class of the web service.
 */
public class WebService extends Application<WebServiceConfiguration> {

    private String name;

    public static void main(final String[] args) throws Exception {
        new WebService().run(args);
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void initialize(final Bootstrap<WebServiceConfiguration> bootstrap) {
    }

    @Override
    public void run(final WebServiceConfiguration config, final Environment environment) throws Exception {
        name = config.getName();
        environment.jersey().register(new UUIDMessageBodyReader());
        environment.jersey().register(new UUIDMessageBodyWriter());
        environment.jersey().register(new DocumentResource());

        environment.healthChecks().register("name", new HealthCheck() {
            @Override protected Result check() throws Exception {
                return Result.healthy(name);
            }
        });
    }


}
