package net.interview;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Configuration description for the web service.
 */
public class WebServiceConfiguration extends Configuration {

    @NotEmpty
    @JsonProperty
    private final String name = null;

    public String getName() {
        return name;
    }
}
