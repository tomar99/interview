package net.interview.io;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Scanner;
import java.util.UUID;

public class UUIDMessageBodyReader implements MessageBodyReader<UUID> {
    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type.equals(UUID.class);
    }

    @Override
    public UUID readFrom(Class<UUID> type, Type genericType, Annotation[] annotations, MediaType mediaType,
                                    MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
                                    throws IOException, WebApplicationException {
        //see http://stackoverflow.com/a/5445161/4704077
        Scanner s = new Scanner(entityStream).useDelimiter("\\A");
        String content = s.hasNext() ? s.next() : "";
        return UUID.fromString(content);
    }
}
